# Statuts Mialt

<!-- Préambule
Ces statuts sont encore en cours de rédaction.
C'est un fork des [statuts de Scopyleft](https://gitlab.com/scopyleft/les-statuts/-/blob/main/statuts.md) dans lesquels ils était mentionné :  
-->

<!-- Question
Remplacer les co-gérantes par la gérances

On doit faire une AG de constitution début mai
-->
Pour rappel, tout ce qui n’est pas prévu dans les présents statuts est régi par les textes en vigueur.

## ︎Forme – dénomination – durée – objet – siège social

### Article 1 - Forme

Société Coopérative de Production (SCOP) à Responsabilité Limitée, à capital variable régie par les textes suivants :

- la loi [n°78-763 du 19 juillet 1978][loi 78] portant statut des sociétés coopératives de production et ses décrets d'application;
- la loi [n°47-1775 du 10 septembre 1947][loi 47] portant statut de la coopération ;
- le [Chapitre III (Titre II, Livre II) du Code de commerce][chapitre III], et plus particulièrement les articles [L223-1 à L223-43][article L223], [R223-1 à R223-36][article R223], [L231-1][article L231-1 à L 231-8] et R 210 -1 et suivants;
- les [articles 1832 à 1844-17 du Code Civil][articles 1832 a 1844].

[loi 78]: https://www.legifrance.gouv.fr/loda/id/JORFTEXT000000339242/

[loi 47]: https://www.legifrance.gouv.fr/loda/id/JORFTEXT000000684004/

[chapitre III]: https://www.legifrance.gouv.fr/codes/id/LEGISCTA000006146044/

[articles 1832 a 1844]: https://www.legifrance.gouv.fr/codes/id/LEGIARTI000006444158/

[article L223]: https://www.legifrance.gouv.fr/codes/section_lc/LEGITEXT000005634379/LEGISCTA000006146044/

[article R223]: https://www.legifrance.gouv.fr/codes/section_lc/LEGITEXT000005634379/LEGISCTA000006146228/

[article L231-1]: https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000006228803



### Article 2 - Dénomination

Mialt

Tout acte et document émanant de la société et destiné aux tiers doit porter la dénomination sociale, précédée ou suivie immédiatement et lisiblement des mots « société coopérative de production à responsabilité limitée, à capital variable ».


### Article 3 - Durée de la société

La durée de la société est fixée à 99 ans, à compter du jour de son immatriculation au registre du Commerce et des Sociétés, sauf dissolution anticipée ou prorogation. 

### Article 4 - Objet

<!-- Documentation
Il est recommandé de ne pas faire apparaître les valeurs dans les statuts — même en préambule — mais de le faire dans un document à part qui peut évoluer de manière autonome (sans déclaration au Greffe).

Il faut essayer d’être le plus précis possible pour accéder à un code NAF (garder la complexité/contexte en préambule). Si un code NAF est visé en particulier, il est recommandé de reprendre les termes de la descriptions pour coller au mieux aux attentes. 
-->


#### Définition

MIALT a pour objet de concevoir, produire et distribuer des boissons et aliments exclusivement sans produits d'origine animale et en adéquation avec les limites planétaires. Nous mettons au cœur de notre démarche le fait de prendre soin des êtres humains et de l'environnement.

<!-- 
Mialt a pour objectif la création d'un lieu convivial et attractif à Montpellier, dans lequel on retrouvera : 
- des boissons fermentées bio à partir d'ingrédients locaux et en circuit court (bières, kombucha, kéfir, etc.) produites sur place ;
- une cuisine locale, de saison et sans produits d'origine animale ;
- des évènements culturels et pédagogiques (théâtre d'improvisation, stand-up, concerts, conférences, etc.)
-->

#### Mise en œuvre

Acquisition et exploitation de fonds de commerce dont les activités commerciales sont les suivantes :

- production et distribution, sur places ou à emporter, de boissons et de repas
- service de traiteur pour des évènements publics ou privés
- organisation d'évènements culturels et éducatifs
- adhésion et participation aux outils financiers et aux structures du Mouvement Scop.

Et toutes activités annexes, connexes ou complémentaires s’y rattachant directement ou indirectement, ainsi que toutes opérations civiles, commerciales, industrielles, mobilières, immobilières, de crédit, utiles directement ou indirectement à la réalisation de l’objet social.

<!-- Question
Est-ce qu'il faut faire une double inscription à la chambre des commerces pour le traiteur ?
-->

<!-- Documentation
Il est recommandé de terminer par le paragraphe ci-dessus pour permettre de réaliser toutes les activités connexes à nos activités principales (acheter des locaux, du matériel, etc.).
-->

### Article 5 - Siège social

159 rue Jacques Monod, 34070 Montpellier

Le transfert du siège social en tout autre endroit du territoire français est effectué par décision de la gérance et ratifié lors de la prochaine Assemblée Générale la plus proche.

<!-- Documentation
Par commodité et formalisme, il est recommandé de faire apparaitre dans les décisions extraordinaires le changement de siège social.
L’indiquer clairement permet de faciliter le travail des personnes aux greffes qui n’auront pas à chercher l’information.

Pour les SARL il n’y a pas de choix possible concernant ce sujet, c’est la loi qui fixe cette modalité.
Soit c’est une décision prise par la gérance ratifiée en AG, soit c’est décidé collectivement pendant une AG.

La ratification est permise par la lois qui s’est assouplie pour permettre au co gérance d’effectuer les démarches administratives sans attendre ou convoquer en AG.
-->

## Capital social et souscriptions au capital

### Article 6 - Capital social et apports

Le capital social initial est fixé à 10 005 € divisé en 667 parts de 15 € chacune entièrement souscrites et libérées, réparties entre les associé·es en proportion de leurs apports.
Les soussigné·es, dont les noms suivent, apportent à la Société :

- Mme. Audrey BRAMY, née le 13/09/1991 aux Lilas (93), pacsée, domiciliée au 159 rue Jacques Monod à Montpellier (34), 4 500 € représentés par 300 parts sociales ;
- M. Arthur LENOIR, né 13/07/1987 à Paris 14 (75), pacsé, domicilié au 159 rue Jacques Monod à Montpellier (34),  4 500 € représentés par 300 parts sociales ;
- Mme Hélène QUEYSSALIER, née le 19/02/1996 à Pontoise (95), célibataire, domiciliée au 60 rue Dora Maar (Bâtiment D) à Montpellier (34), 1 005 € représentés par 67 parts sociales ;


Soit un total de 10 005 € représentant le montant intégralement libéré des parts, laquelle somme a été régulièrement déposée le 12 juin 2023 à un compte ouvert au nom de la Société en formation à la banque Crédit Coopératif ainsi qu’il en est justifié au moyen du récépissé établi par la banque dépositaire.

<!-- Documentation
La répartition des parts sociales est mentionnée dans les statuts. — [article 7 du code du commerce][article 7]
[article 7]: https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000006222923/2022-08-17
-->

### Article 7 - Variabilité du capital

Le capital social est variable. Il est susceptible d’augmentation par des versements successifs des associé·es ou l’admission d’associé·es nouveaux et nouvelles.

Il peut diminuer à la suite de démissions, exclusions, décès, décisions de remboursement d’associé·es extérieur·es ou remboursements partiels, sous réserve des limites et conditions prévues ci-après.

Il n’est pas obligatoire de préciser le montant maximal du capital, comme indiqué dans [l’article 7 de la loi du 10 septembre 1947][article 7 du 10 septembre 1947].

[article 7 de la loi du 10 septembre 1947]: https://www.legifrance.gouv.fr/loda/article_lc/LEGIARTI000038590007

### Article 8 - Capital minimum

Le capital social ne peut être inférieur à 4 000 €.

Il ne peut être réduit du fait de remboursements à moins du quart du capital le plus élevé atteint depuis la constitution de la Scop.

Les associé·es extérieur·es tel·les que défini·es infra ne peuvent détenir plus de 49 % du capital social.

### Article 9 - Parts sociales et bulletins de souscriptions

Les parts sociales sont nominatives et indivisibles. La Société ne reconnaît qu’un·e propriétaire pour chacune d’elle. 
Leur valeur est uniforme. Elles doivent être intégralement libérées dès leur souscription.
Aucun·e associé·e n’est tenu·e de souscrire et libérer plus d’une seule part lors de son admission.
Toute souscription de parts sociales donne lieu à la signature d’un bulletin de souscription par l’associé·e et à la remise à celui-ci d’un certificat de parts.

Les cessions de parts sociales entre associé·es sont soumises à l’agrément de la gérance. La cession ne peut avoir pour effet de réduire le nombre de parts détenues par un·e associé·e en dessous du nombre résultant des engagements auxquels il ou elle peut être tenu·e du fait des présents statuts.

Aucun·e associé·e ne peut détenir seul·e plus de 50 % des parts sociales.
La responsabilité de chaque associé·e, détenteur ou détentrice de parts est limitée à la valeur des parts qu’il ou elle a souscrites ou acquises.

### Article 10 - Engagement de souscription des associé·es salarié·es

Si l'associé·e est lié·e à la Société par un contrat de travail ou par un mandat social, il ou elle s'engage à souscrire et à libérer, pour chaque exercice des parts sociales pour un montant égal à 2 % de la rémunération brute soumise à cotisations sociales perçue de la Société au cours de l’exercice. Cet engagement cessera lorsque le montant du capital total de l'associé·e atteindra une somme équivalente à 1 mois de salaire brut perçu de la coopérative.
<!-- Documentation
Chaque mois 2% de la rémunération brute sont mis en compte courant d'associés.
Lors de l'AGO, lors d'une point annuel, on fait signer un bulletin de souscription avec un équivalent des montants mis en compte courrant d'associé·es en parts sociales.

Quand le salaire brut augmente l'engagement de souscription augmente aussi.
-->

Toutefois, l’Assemblée Générale peut, par délibération dûment motivée prise au début de l’exercice social, fixer les engagements prévus à l’alinéa 1<sup>er</sup> à un montant inférieur.

L’engagement de souscription prend effet à la date d’admission au sociétariat.

En cas d’ouverture d’une procédure de liquidation amiable, redressement ou liquidation judiciaire de la Société, ou en cas de démission, exclusion ou décès de l’associé·e, celui ou celle-ci ou ses ayants droit, ne sont plus tenu·es de souscrire de nouvelles parts à compter du fait générateur.

<!--
Par exemple pour un SMIC brut (dans le cas général) 1709,28*0,01 = 34,2 euros par mois sont prélevés sur le salaire.
Pour atteindre un smic brut, il lui faudra soucrire pendant une durée de 50 mois, soit 4 ans et un trimestre.
-->

### Article 11 - Exécution des engagements de souscription

Pour l'exécution des engagements prévus à l'article ci-dessus, il est pratiqué sur le salaire perçu par tout associé·e, une retenue égale au pourcentage fixé par les statuts ou décidé chaque année.

À la fin de chaque exercice, l'associé·e souscrit des parts pour un montant égal aux retenues opérées qui sont affectées à la libération intégrale des parts sociales ainsi souscrites. Cette souscription est constatée par la signature d’un bulletin de souscription.

L’associé·e pourra également réaliser son engagement de souscription par rachat de parts sociales à un·e associé·e, dans les conditions prévues par les statuts pour les cessions de parts sociales, ou par tout autre moyen.

<!-- Documentation
En pratique, une retenue sur salaire est effectuée chaque mois.
A la fin de l'exercice, lors de l'AGO on acte la souscription des parts pour le montant égale aux sommes retenues sur le salaire durant l'exercice.
On ouvre la possibilité de souscrire à des part autrement.

En cas de libération des parts au moyen de retenues sur les rémunérations, ces retenues ne peuvent excéder le plafond prévu à l'article L. 144-2 du code du travail pour le remboursement des avances consenties par l'employeur.
-->

### Article 12 - Autres souscriptions

<!-- Documentation
Dans les accords de participations, on peut définir des règles x% de la participation remonte en capital jusqu'à avoir atteint mon engagement en souscription.
-->

#### 12.1 - Souscriptions complémentaires effectuées par les associé·es employé·es dans la Société.

Ces souscriptions doivent être libérées immédiatement, soit par l’emploi de leurs droits sur la répartition des excédents ou résultant d’un accord de participation prévoyant la possibilité d’affectation des droits en parts sociales, soit par le déblocage anticipé de tout ou partie de leurs droits à participation, soit par l’affectation à la création de nouvelles parts sociales décidée par l’_Assemblée Générale Ordinaire_, des répartitions de bénéfices revenant aux associé·es.

#### 12.2 - Souscriptions à une émission de parts sociales réservée aux salariés.

Ces souscriptions sont décidées par l’_Assemblée Générale Ordinaire_ qui fixe, ou charge la gérance d’en fixer les conditions, notamment d’ancienneté requise des souscripteurs, de délais de libération et, le cas échéant, de versements complémentaires de la Société.
Un salarié ne peut, au cours de la même année civile, souscrire à l’émission réservée que dans la limite d’un montant correspondant à la moitié du plafond annuel de la sécurité sociale.

#### 12.3 - Adhésion et souscription à un plan d’épargne d’entreprise

Les souscriptions complémentaires peuvent intervenir dans le cadre d’une adhésion ou d’une souscription à un plan d’épargne entreprise, lorsque les avoirs, y compris ceux résultant du placement des droits à participation, peuvent être investis en parts sociales de la Société.

#### 12.4 - Autres souscriptions

Les autres souscriptions sont celles effectuées par les associés employés ou non dans la Société, après autorisation de la gérance.

### Article 13 - Annulation des parts sociales

Les parts sociales des associé·es démissionnaires, exclu·es, décédé·es, ou à qui il a été décidé de faire perdre la qualité d’associé·e, et celles détenues par des associé·es au-delà des plafonds prévus par les présents statuts sont annulées.

Les sommes qu'elles représentent sont assimilées à des créances ordinaires et remboursées dans les conditions prévues par les présents statuts.

Sauf le cas prévu à l’article 16.5 et nonobstant les modalités de remboursement, les parts sont annulées au jour de la perte de la qualité d’associé·e ou de la demande de remboursement partiel.

## Acquisition et perte de la qualité d’associé·e

### Article 14 - Admission au sociétariat

Les contrats de travail conclus par la Société doivent être écrits et doivent prévoir que tout travailleur·euse doit présenter sa candidature comme associé·e, au terme d’un délai de 1 année après son entrée en fonction.

Le·a candidat·e est considéré·e comme associé·e à la date de l’assemblée générale suivant le dépôt de la candidature auprès des gérant·es, sauf si ladite assemblée des associé·es appelée à statuer sur le rejet de cette candidature, la rejette. 
Le rejet de la candidature doit avoir été mis à l’ordre du jour. La majorité requise pour l’adoption du rejet de candidature est la majorité requise pour la modification des statuts.
<!-- Documentation
**Majorité des Décisions Extraordinaires**  
- première consultation :  
    - Quorum : les trois quarts du total des droits de vote.
    - Majorité : les trois quarts du total des droits de vote présents ou représentés.
  
- Deuxième consultation :  
    - Quorum : la moitié du total des droits de vote.
    - Majorité : les trois quarts du total des droits de vote présents ou représentés.
-->
<!-- Question
Est-ce qu'on reste bien sur 1 an ou on va sur 18 mois.
-->

Si la candidature n'a pas été présentée au terme du délai ci-dessus, l'intéressé·e est réputé·e démissionnaire de son emploi trois mois après mise en demeure, restée infructueuse de la gérance.

Le ou la salarié·e qui présente sa candidature avant le terme du délai prescrit entre dans le cadre de la candidature volontaire et les dispositions ci-après sont applicables :

- Si le ou la candidat·e est employé·e dans la Société depuis moins d’un an à la date de sa candidature, la gérance peut agréer ou rejeter la demande. Si elle l’agrée, elle la soumet à la prochaine _Assemblée Générale Ordinaire_ qui statue à la majorité ordinaire.
- Le ou la salarié·e qui a n’a pas été admis·e n’est pas dispensé·e de représenter sa candidature dans le cadre de sa candidature obligatoire.

Le candidat peut présenter sa candidature tous les 6 mois.

Seul·es les salarié·es ou les gérant·es de la SCOP peuvent devenir associé·es.

### Article 15 - Perte de la qualité d’associé·e

#### 15.1 - Par la démission du sociétariat
Cette démission prend effet immédiatement dès sa notification à la gérance.
Si elle est donnée par un·e associé·e salarié·e dans la Société, celui ou celle-ci est par conséquent réputé·e démissionnaire de son contrat de travail dès notification de sa démission du sociétariat.

#### 15.2 - Par la rupture du contrat de travail 
La perte de la qualité d’associé·e intervient dès la notification de la rupture du contrat de travail par la partie qui en a pris l’initiative (date de première présentation de la lettre recommandée ou de la remise en main propre contre décharge) et si la rupture du contrat de travail intervient par accord des parties, à la date de prise d’effet de la rupture.

Modes de rupture du contrat de travail ne faisant pas perdre la qualité d’associé·e :

- La mise à la retraite ;
- Le licenciement pour motif économique ;
- L'invalidité rendant l'intéressé·e inapte au travail.

Tous les autres modes de rupture du contrat de travail font perdre la qualité d’associé·e.

Dans le cas où l’associé·e salarié·e a fait part à la gérance de sa demande de conserver la qualité d’associé·e, une assemblée devra être convoquée avant la fin du préavis. Si l’assemblée refuse le maintien de la qualité d’associé·e, ce dernier sera réputé avoir perdu cette qualité à la date de notification de la rupture de son contrat de travail.

<!-- Documentation
Les ancien·nes salarié·es deviennent alors des associé·es non employé·es ou extérieur·es auxquel·les il est possible de faire perdre la qualité d’associé·e, sur décision de l’assemblée des associé·es.
-->

#### 15.3 - Par le décès de la personne physique ou extection de la personne morale

#### 15.4 - Perte par décision de l’Assemblée Générale, pour l’associé·e non salarié·e
L’assemblée générale statue aux conditions de majorité ordinaire pour faire perdre la qualité d’associé·e à un·e associé·e qui n’est plus employé·e dans la Société. 

#### 15.5 - Perte par exclusion prononcée par l’Assemblée Générale
L'Assemblée Générale, statuant dans les conditions de majorité prévues pour les décisions extraordinaires, peut toujours exclure un·e associé·e qui aura causé un préjudice matériel ou moral à la Société.

Le fait qui entraîne l'exclusion est constaté par la gérance, habilitée à demander toutes justifications à l'intéressé·e.

Une convocation spéciale de l'Assemblée doit être adressée à celui ou celle-ci pour qu'iel puisse présenter sa défense. 

L'assemblée apprécie librement l'existence du préjudice causé à la Société.
La perte de la qualité d'associé·e intervient dans ce cas à la date de l'assemblée qui a prononcé l'exclusion.

#### 15.6 - Par la non-réalisation de l’engagement de souscription
L'associé·e qui, de son fait, est en retard de plus de six mois dans l'exécution de l’engagement de souscription statutaire, et de la signature du bulletin de souscription correspondant, est considéré·e de plein droit comme démissionnaire du sociétariat, trois mois après avoir été invité·e à se mettre en règle par lettre recommandée avec accusé de réception, s'il n'a pas régularisé dans ce délai.
La démission prend effet automatiquement trois mois après l’envoi de la lettre. L'intéressé·e doit être informé·e dans la lettre de mise en demeure, qu’à défaut de régularisation, il ou elle sera également réputé·e démissionnaire de son contrat de travail de plein droit.

<!-- Documentation
Ce que dit la loi, [article 8 de la Loi n° 78-763 du 19 juillet 1978][article 8] portant statut des sociétés coopératives de production.

[article 8]: https://www.legifrance.gouv.fr/loda/article_lc/LEGIARTI000029321329
-->

### Article 16 - Remboursement des parts sociales des ancien·nes associé·es et remboursements partiels des associé·es

#### Article 16.1 - Remboursements partiels demandés par les associé·es 

La demande de remboursement partiel est faite auprès de la gérance par lettre recommandée avec accusé de réception ou remise en main propre contre décharge. 

Les remboursements partiels sont soumis à autorisation préalable de l’assemblée des associés statuant à la majorité ordinaire.
Ils ne peuvent concerner que la part de capital excédant l’engagement statutaire de souscription lorsqu’il est prévu par les statuts.

Les parts sociales souscrites dans le cadre de l’épargne salariale sont remboursables, dans les conditions légales sur simple demande, selon les modalités ci-après.

#### Article 16.2 - Montant des sommes à rembourser

##### Date d’évaluation

Le montant du capital à rembourser est arrêté à la date de clôture de l'exercice au cours duquel la perte de la qualité d'associé·e est intervenue ou au cours duquel l’associé·e a demandé un remboursement partiel de son capital social.

##### Valeur de remboursement

Les associé·es n'ont droit qu'au remboursement du montant nominal de leurs parts sociales, sous déduction des pertes éventuelles apparaissant à la clôture de l'exercice.

##### Calcul de la valeur de remboursement en cas de pertes

Pour le calcul de la valeur de remboursement de la part sociale, il est convenu que les pertes qui apparaissent à la clôture de l’exercice s'imputent pour partie sur les réserves statutaires et pour partie sur le capital.

<!-- Documentation
Kesako ?
L’imputation sur la réserve légale est interdite.
Les réserves statutaires sont principalement constituées par le fonds de développement.
Le montant des pertes à imputer sur le capital se calcule selon la formule suivante :
`Perte x [capital / (capital + réserves statutaires)].`
 - le montant du capital à retenir est celui du dernier jour de l’exercice auquel a été réintégré le montant du capital qui était détenu par les associés sortants ;
 - les réserves statutaires sont celles inscrites au bilan au dernier jour de l’exercice.
 -->

#### Article 16.3 - Pertes survenant dans un délai de cinq ans

S'il survenait dans un délai de cinq années suivant la perte de la qualité d'associé·e, des pertes se rapportant aux exercices durant lesquels l'intéressé·e appartenait à la Société, la valeur du capital à rembourser serait diminuée proportionnellement à ces pertes.

Au cas où tout ou partie des parts de l'ancien·ne associé·e auraient déjà été remboursées, la Société serait en droit d'exiger le reversement du trop perçu.

#### Article 16.4 - Ordre chronologique 

Les remboursements ont lieu dans l'ordre chronologique où ont été enregistrées les pertes de la qualité d'associé·e ou la demande de remboursement partiel. Il ne peut être dérogé à l’ordre chronologique, même en cas de remboursement anticipé.

#### Article 16.5 - Suspension des remboursements

Les remboursements ne peuvent avoir pour effet de réduire le capital à un montant inférieur au quart du capital maximum atteint depuis la constitution de la Scop ou de sa transformation en Scop. 

Dans ce cas, l'annulation et le remboursement des parts sociales ne sont effectués qu'à concurrence de souscriptions nouvelles permettant de maintenir le capital au moins à ce minimum.

L’ancien·ne associé·e dont les parts sociales ne peuvent pas être annulées devient détenteur de capital sans droit de vote. Il ne participe pas aux assemblées d’associé·es. La valeur de remboursement de la part sociale est calculée à la clôture de l’exercice au cours duquel les parts sociales sont annulées.

#### Article 16.6 - Délai de remboursement

Les ancien·nes associé·es ou les associé·es ayant demandé un remboursement partiel ne peuvent exiger, avant un délai de cinq ans, le règlement des sommes qui leur sont dues, sauf décision de remboursement anticipé prise par l’assemblée des associés statuant à la majorité ordinaire.
<!-- Documentation
5 ans est un délais légal permettant de laisser du temps à l'entreprise d'avoir la trésorerie.
Permet de protéger la société coopérative.
Mais l'assemblée peut tout à fait dire qu'on rembourse plus rapidement.

-->

Le délai court à compter de la date de la perte de la qualité d’associé·e ou de la réception de la demande de remboursement par la gérance.

Le montant dû aux ancien·nes associé·es, ou aux associé·es ayant demandé un remboursement partiel, porte intérêt à un taux fixé par l'assemblée des associés et qui ne peut être inférieur au taux du livret A du 31 décembre de l'exercice précédent.

#### Article 16.7 - Héritier·es et ayants droit

Les dispositions du présent article sont applicables aux héritier·es et ayants droit de l'associé·e décédé·e.

## Administration et contrôle

### Article 17 - La gérance

La Société est administrée par trois gérant·es personnes physiques désigné·es par l’assemblée générale des associés à bulletins secrets.

Les premier·es gérant·es de la Société seront nommé·es dans l’AG consitutive.
Leurs fonctions expireront lors de l'assemblée générale appelée à statuer sur les comptes clos au 31 décembre 2027 sous réserve de la faculté de réélection prévue ci-dessous.

<!--
Dans les rapports entre associé·es, les co-gérant·es peuvent faire tout acte de gestion dans l’intérêt de la société ([Articles L223-18][article L223-18] et [L223-29][article L223-29] du Code du Commerce).

[article L223-18]: https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000031013015
[article L223-29]: https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000038799356

Les actes de gestion font l’objet d’un contrôle des associé·es, lors de l’_Assemblée Générale Ordinaire_ annuelle conformément à l’[article 8 de la loi de 1947][article 8].

[article 8]: https://www.legifrance.gouv.fr/loda/article_lc/LEGIARTI000035255562
-->

### Article 18 - Obligations et droits des gérant·es

Iels doivent être associé·es. Les deux tiers des gérant·es doivent être employé·es de l’entreprise. En cas de gérant·e unique, iel est obligatoirement employé·e de l’entreprise.

<!-- Documentation
Le sociétariat étant réservé aux salarié·es (sauf à la création), la gérance est réservée aux salariées.
-->

S'iels n'ont pas conclu un contrat de travail avec la Société, ou si, du fait de l'exercice de leur mandat, iels ne peuvent exercer les fonctions prévues à ce contrat, les gérant·es percevant une rémunération au titre de leur mandat social sont considéré·es, conformément à l’[article 17 de la loi du 19 juillet 1978][article 17], comme travailleurs·euses employé·es de la Société au regard des présents statuts et de l'application de la législation du travail et de la sécurité sociale.

[article 17]: https://www.legifrance.gouv.fr/loda/article_lc/LEGIARTI000029321325

### Article 19 - Durée des fonctions

#### 19.1 - Nomination

Les gérant·es sont choisi·es par les associé·es pour une durée de 4 ans. La nomination est prononcée à la majorité du nombre total des voix sur première convocation et à la majorité des voix des associé·es présent·es ou représenté·es sur deuxième convocation.

Iels sont rééligibles et révocables.

Leurs fonctions prennent fin à l’issue de l’_Assemblée Générale Ordinaire_ tenue dans l’année au cours de laquelle expire leur mandat.

<!-- Documentation
#### Article 25.4 - Modalités à suivre en cas de modification des statuts ou de dissolution de la SCOP

Les modifications des statuts sont effectuées conformément aux dispositions légales et font l’objet d’une approbation à l’occasion d’une _Assemblée Générale Extraordinaire_.
-->

<!-- Documentation
Nécessité d’accord unanime des associé·es pour la modification des statuts : [article 1836 du Code Civil](https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000006444069)
-->

<!-- Documentation
La dissolution de la SCOP s’opère conformément aux dispositions légales et notamment à l’[article 1844-5 du Code Civil][article 1844-5] et l’[article 5 de la Loi n° 78-763 du 19 juillet 1978][article 5].
Concernant la répartition de l’actif net subsistant après extinction du passif, il sera fait référence à l’[article 19 de la loi n° 47-1775 du 10 septembre 1947][article 19].

[article 1844-5]: https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000006444165/
[article 5]: https://www.legifrance.gouv.fr/loda/id/JORFTEXT000000339242/
[article 19]: https://www.legifrance.gouv.fr/loda/id/JORFTEXT000000684004/
-->

#### 19.2 - Révocation

La révocation est prononcée à la majorité du nombre total des voix.

### Article 20 - Pouvoirs de la gérance

La gérance dispose de tous les pouvoirs nécessaires pour agir en toutes circonstances au nom de la société dans les limites de son objet social sous la réserve des pouvoirs conférés à l'assemblée des associé·es par la loi et les statuts.

En cas de pluralité de la gérance, chacun·e des gérant·es dispose de l’intégralité des pouvoirs.

### Article 21 - Conseil de surveillance 
Si le nombre d'associé·es est supérieur à vingt à la clôture du dernier exercice, un conseil de
surveillance doit être constitué conformément à l'[article 16 de la loi du 19 juillet 1978](https://www.legifrance.gouv.fr/loda/article_lc/LEGIARTI000029321001), l'assemblée des associé·es étant convoquée à cet effet, dans les plus
brefs délais par la gérance.

<!-- Documentation 
   Article 16 de la loi du 19 juillet 1978

   Lorsque la société coopérative de production est constituée sous la forme d'une société à responsabilité limitée ou d'une société par actions simplifiée, les gérants ou les membres de l'organe de direction sont nommés par l'assemblée des associés, pour une durée qui ne peut excéder quatre ans.

    Si elle compte plus de vingt associés, un conseil de surveillance est constitué ; il est composé de trois membres au moins et de neuf membres au plus, désignés par l'assemblée des associés et en son sein, pour une durée que les statuts déterminent et qui ne peut excéder quatre ans.

    Les fonctions de gérant ou de membre de l'organe de direction et de membre du conseil de surveillance sont incompatibles.

    Les gérants ou les membres de l'organe de direction et les membres du conseil de surveillance sont, sauf stipulations contraires des statuts, rééligibles ; ils peuvent être révoqués à tout moment par l'assemblée des associés, même si la question n'a pas été inscrite à l'ordre du jour.

    Le conseil de surveillance exerce le contrôle permanent de la gestion de la société par les gérants ou les membres de l'organe de direction.

    A toute époque de l'année, il opère les vérifications et les contrôles qu'il juge opportuns et peut se faire communiquer tout document qu'il estime utile à l'accomplissement de sa mission ou demander au gérant un rapport sur la situation de la société.

    Il présente à l'assemblée des associés un rapport sur la gestion de la société.

    Les statuts peuvent subordonner à son autorisation préalable la conclusion des opérations qu'ils énumèrent.

    La responsabilité des membres du conseil de surveillance est soumise aux dispositions de l'article L. 225-257 du code de commerce.
-->

### Article 22 - Commissaires aux comptes

Conformément à l'[article L.223-35 du code du commerce](https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000038838163) les associé·es peuvent nommer un ou plusieurs commissaires aux comptes dans les conditions prévues à l'article [L. 223-29](https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000038799356).

<!-- Documentation

    Article L.223-35 du code de commerce.

    Les associés peuvent nommer un ou plusieurs commissaires aux comptes dans les conditions prévues à l'article L. 223-29.

    Sont tenues de désigner un commissaire aux comptes au moins les sociétés à responsabilité limitée qui dépassent à la clôture d'un exercice social des chiffres fixés par décret pour deux des critères suivants : le total de leur bilan, le montant hors taxes de leur chiffre d'affaires ou le nombre moyen de leurs salariés au cours d'un exercice.

    Même si ces seuils ne sont pas atteints, la nomination d'un commissaire aux comptes peut être demandée en justice par un ou plusieurs associés représentant au moins le dixième du capital.

    Sont également tenues de désigner un commissaire aux comptes, pour un mandat de trois exercices, les sociétés dont un ou plusieurs associés représentant au moins le tiers du capital en font la demande motivée auprès de la société.

    Conformément à l’article 20 de la loi n° 2019-486 du 22 mai 2019 :

    II.-Les présentes dispositions s'appliquent à compter du premier exercice clos postérieurement au 27 mai 2019, lendemain de la publication du décret n° 2019-514 du 24 mai 2019.

    III.-Les seuils fixés par les décrets prévus aux articles L. 221-9, L. 223-35, L. 227-9-1, L. 225-218, L. 226-6 et L. 823-2-2 du code de commerce, dans leur rédaction résultant du présent article, sont applicables aux entreprises fiscalement domiciliées dans une collectivité d'outre-mer régie par l'article 73 de la Constitution à compter du 1er janvier 2021
-->

### Article 23 - Révision coopérative

<!-- Documentation

    Article 54 - Version en vigueur depuis le 02 août 2014
    Modifié par LOI n°2014-856 du 31 juillet 2014 - art. 30

    Les sociétés coopératives de production sont tenues, indépendamment des obligations imposées à toutes les entreprises, et sous peine de la sanction prévue à l'article 23 de la loi n° 47-1775 du 10 septembre 1947 portant statut de la coopération, de fournir aux services de l'inspection du travail, toutes justifications utiles permettant de vérifier qu'elles fonctionnent conformément à la présente loi.

    Aucune société ne peut prendre ou conserver l'appellation de société coopérative de production ou de société coopérative de travailleurs, société coopérative ouvrière de production ou société coopérative et participative ou utiliser cette appellation ou les initiales " SCOP ", et prétendre au bénéfice des dispositions prévues par les textes législatifs ou réglementaires relatifs aux sociétés coopératives de production si elle n'est pas inscrite, après production des pièces justificatives nécessaires, sur une liste dressée par le ministère du travail dans les conditions fixées par décret.
    Toute personne intéressée peut demander au président du tribunal compétent statuant en référé d'enjoindre, le cas échéant sous astreinte, à toute personne concernée de supprimer les mots : " société coopérative de production ”, " société coopérative de travailleurs ”, " société coopérative ouvrière de production " ou " société coopérative et participative " ou les initiales : " SCOP ”, lorsque cette appellation est employée de manière illicite malgré l'interdiction édictée au deuxième alinéa.

    Le président du tribunal peut, en outre, ordonner la publication de la décision, son affichage dans les lieux qu'il désigne, son insertion intégrale ou par extraits dans les journaux et sa diffusion par un ou plusieurs services de communication au public en ligne qu'il indique, le tout aux frais des dirigeants de l'organisme ayant utilisé la dénomination en cause.

-->

#### 23.1 Périodicité

La Société fera procéder tous les ans à la révision coopérative prévue par l’[article 54 bis de la loi n°78-763 du 19 juillet 1978][article 54bis] portant statut des sociétés coopératives de production, par le décret n°2015-706 du 22 juin 2015 et le décret 2015-806 du 1<sup>er</sup> juillet 2015. Le réviseur devra procéder également à l’examen analytique de la situation financière, de la gestion et des compétences collectives de la société.

[article 54bis]: https://www.legifrance.gouv.fr/loda/article_lc/LEGIARTI000029320897

<!-- 
En outre, la révision coopérative devra intervenir sans délai si :
- trois exercices consécutifs font apparaître des pertes comptables ;
- les pertes d'un exercice s'élèvent à la moitié au moins du montant le plus élevé atteint par le capital ; 
- elle est demandée par le dixième des associés ; 
- elle est demandée par le ministre chargé de l’économie sociale et solidaire ou tout ministre compétent à l’égard de la coopérative en question.

La demande est adressée aux gérant·es.
-->

#### 23.2 Rapport de révision

Le rapport établi par le réviseur coopératif sera tenu à la disposition des associé·es quinze jours avant la date de l'_Assemblée Générale Ordinaire_. Le réviseur est convoqué à l’Assemblée Générale dans les mêmes conditions que les associé·es. Le rapport sera lu à l'_Assemblée Générale Ordinaire_ ou à une _Assemblée Générale Ordinaire_ réunie à titre extraordinaire, soit par le réviseur s'il est présent, soit par le ou la Président·e de séance. L'Assemblée Générale en prendra acte dans une résolution.

#### 23.3 Révision à la demande d’associé·es

Si l'opération de révision est déclenchée à la demande du dixième des associé·es, une _Assemblée Générale Ordinaire_ réunie à titre extraordinaire sera réunie dans les trente jours qui suivront la date à laquelle le réviseur aura remis son rapport à la Société.

Dans ce cas, la gérance présente obligatoirement un rapport sur la situation de l'entreprise.

## Assemblée des associé·es

### Article 24 - Décisions réservées à l’Assemblée Générale
<!-- Documentation

cf. [article 7 de la loi de 1947][article 7]
cf. [article L223-27 du code du commerce][article L223-27]

[article 7]: https://www.legifrance.gouv.fr/loda/article_lc/LEGIARTI000038590007/
[article L223-27]: https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000038799336/

alinea 1 : l’approbation du rapport de gestion, de l’inventaire et des comptes annuels établis par les gérant·es.
-->
Les décisions relevant de la compétence des associé·es sont prises en Assemblée Générale.

<!-- Documentation
**Compétences de l’assemblée ordinaire**
L’assemblée ordinaire annuelle des associé·es, le cas échéant réunie
extraordinairement pour examiner les questions dont la solution ne souffre pas
d’attendre la prochaine assemblée annuelle, exerce les pouvoirs qui lui sont
conférés par la loi et notamment :
- approuve ou redresse les comptes ;
- ratifie la répartition des bénéfices si une pré-répartition a été décidée par la gérance ;
- prononce, dans les conditions prévues aux statuts, l’admission des associé·es ;
- nomme la gérance, contrôle sa gestion et en révoque les gérant·es ;
- s’il y a lieu, nomme et révoque les membres du conseil de surveillance ;
- approuve les conventions passées entre la Société et les associé·es ;
- décide ou ratifie la répartition des bénéfices et peut décider la conversion
en parts sociales des répartitions revenant aux associé·es ;
- délibère sur toutes questions portées à l’ordre du jour n’emportant pas
modification des statuts ;
- désigne le ou la réviseur·se et son ou sa suppléant·e figurant sur la liste
des réviseur·euses agréé·es ;
- décide les émissions de titres participatifs.
-->
<!-- Documentation
**Compétences de l’assemblée extraordinaire**
L’assemblée des associé·es a compétence pour modifier les statuts, mais ne
peut augmenter les engagements des associé·es sans leur accord unanime,
sauf le cas particulier de l’engagement de souscription au capital
expressément prévu par la loi du 19 juillet 1978.
Elle peut notamment décider ou autoriser, sans que l’énumération ait un
caractère limitatif :
- l’exclusion d’un·e associé·e qui aurait causé un préjudice matériel ou moral à la Société ;
- la modification de la dénomination sociale ;
- la prorogation ou la dissolution anticipée de la Société ;
- la fusion de la Société.
-->
### Article 25 - Dispositions communes aux différentes assemblées

#### 25.1 - Mode de consultation des associé·es

Les associé·es sont réuni·es en assemblées pour prendre des décisions soit à caractère ordinaire, soit à caractère extraordinaire. En aucun cas, les assemblées ne peuvent être remplacées par des consultations écrites. Les associé·es sont réuni·es au moins une fois par an au siège social ou en tout autre lieu précisé par la lettre de convocation.

La SCOP fonctionne sur le principe d’une personne une voix : [Article de 14 de la loi de 1978][Article 14].

<!-- Documentation
    Vérifier que le mode de convocation est intégrer à la loi et le documenter le cas échéant. 
-->
[Article 14]: https://www.legifrance.gouv.fr/loda/article_lc/LEGIARTI000037823137

<!-- Documentation
L'[article 12 de la loi 1978](https://www.legifrance.gouv.fr/loda/article_lc/LEGIARTI000006289834) n'autorise pas la consultation écrite pour les SCOP
-->

L’assemblée générale se compose de tous les associé·es, y compris celles ou ceux admis·es au sociétariat au cours de l’assemblée dès qu’ils ou elles auront été admis·es à participer au vote.

#### 25.2 - Ordre Du Jour

L’ordre du jour est arrêté par l’auteur·e de la convocation.
Un·e ou plusieurs associé·es représentant au moins 5 % des droits de vote peuvent demander, entre le quinzième et le cinquième jour précédant la tenue de l’assemblée, l’inscription à l’ordre du jour de projets de résolutions.
Dans ce cas, la gérance est tenue d’adresser un ordre du jour rectifié à tous les associé·es.

L’assemblée ne peut délibérer que sur les questions portées à l’ordre du jour.
Néanmoins, il peut toujours être procédé à la révocation d’un·e gérant·e même si la question n’a pas été inscrite à l’ordre du jour.

<!-- Documentation
**Feuille de présence**
Il est établi une feuille de présence ou participation sous une autre forme non exclusivement écrite, comportant les nom, prénom et domicile des associé.e.s et le nombre de parts sociales dont chacun.e est titulaire. Elle est signée par tous les associé.e.s présent.e.s, ou participant.e.s tant pour eux-mêmes que pour ceux qu’ils ou elles peuvent représenter.
En cas de réunion dématérialisée, la participation des associé.es et de leur.s éventuel.s pouvoir.s est attestée par tout moyen de signature numérique autorisé par la Loi et apprécié comme de confiance par la collectivité des associé.e.s.
-->

<!-- Document
Ce que dit la lois  
**Comment convoquer une AG**
Les associés sont convoqués par le gérant, ou à défaut par le commissaire aux comptes s’il en existe, La première convocation de toute assemblée générale est faite par lettre simple recommandée ou courrier électronique adressé aux associés quinze jours au moins à l'avance.

La convocation par courrier électronique est subordonnée à l’accord préalable des associés et à la communication de leur adresse électronique. Il est possible de revenir à tout moment sur cet accord en en informant le gérant par lettre recommandée avec demande d’avis de réception.

La lettre de convocation mentionne expressément les conditions dans lesquelles les associés peuvent voter à distance.

Si, pour quelque cause que ce soit, la société se trouve dépourvue de gérant ou si le gérant unique est placé en tutelle, le commissaire aux comptes s’il existe ou tout associé convoque l'assemblée des associés à seule fin de procéder, le cas échéant, à la révocation du gérant unique et, dans tous les cas, à la désignation d'un ou de plusieurs gérants. Dans cette hypothèse, le délai de convocation est réduit à huit jours. 

Un ou plusieurs associés détenant la moitié des droits de vote ou s’ils représentent au moins le dixième des associés et le dixième des droits de vote, peuvent demander la réunion d’une assemblée générale. La demande est adressée au dirigeant qui doit procéder à la convocation dans le délai d’un mois suivant la réception.

**Où se passe une AG**
Les convocations doivent mentionner le lieu de réunion de l’assemblée.

Celui-ci peut être le siège de la Société ou tout autre local situé dans la même ville, ou encore tout autre lieu approprié pour cette réunion, dès lors que le choix qui est fait par le gérant de ce lieu de réunion n’a pas pour but ou pour effet de nuire à la réunion des associés.
-->

#### 25.3 - Vote

La désignation des gérant·es a lieu au scrutin secret. 
Pour toutes les autres questions, il est procédé par vote à main levée, sauf si la majorité de l’assemblée décide le contraire.
En cas de réunion dématérialisée, le vote a lieu par des moyens numériques de confiance.

<!-- Documentation
Les délibérations des assemblées générales sont constatées par des procès-verbaux portés sur un registre spécial et signés par la gérance
-->

<!-- Ajouter le vote par anticipation à distance ?


Vote par anticipation à distance 

Tout associé peut voter par correspondance dans les conditions suivantes : à compter de la convocation de l’assemblée, un formulaire de vote à distance et ses annexes sont remis ou adressés, aux frais de la société, à tout associé qui en fait la demande par lettre recommandée avec demande d’avis de réception.

La société doit faire droit à toute demande déposée ou reçue au siège social au plus tard six jours avant la date de réunion. Le formulaire de vote à distance doit comporter certaines indications fixées par les articles R.225-76 et suivants du code de commerce. Compte tenu de la forme SARL de la société et des règles de vote prévues à l’article L.223-27 du code de commerce, le formulaire doit informer l’associé de façon très apparente que toute abstention exprimée dans le formulaire ou résultant de l’absence d’indication de vote sera assimilée à un vote défavorable à l’adoption de la résolution. Le formulaire peut, le cas échéant, figurer sur le même document que la formule de procuration. Dans ce cas, ce sont les dispositions de l’article R.225-78 du Code de commerce qui sont applicables.

Sont annexés au formulaire de vote par correspondance les documents prévus à l’article R.225-76 du Code de commerce. 

Le formulaire de vote par correspondance adressé à l’associé pour une assemblée vaut pour toutes les assemblées successives convoquées avec le même ordre du jour.

Les formulaires de vote par correspondance doivent être reçus par la société trois jours avant la réunion.

-->

#### 25.4 - Participation des associé·es à l’Assemblée Générale par visioconférence

Les associé·es qui participent aux _Assemblées Générales_ par visioconférence (ou par des moyens de télécommunication permettant leur identification et dont la nature et les conditions d’application sont déterminées par les articles [L225-107][article L225-107]) et [R223-20-1][Article R223-20-1] du Code de commerce sont réputé·es présent·es pour le calcul du quorum et de la majorité.

[article L225-107]: https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000038799438/
[Article R223-20-1]: https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000020316735

<!-- Documentation
Voir aussi [article 10] de la loi de 1947.

[article 10]: https://www.legifrance.gouv.fr/loda/article_lc/LEGIARTI000029320806
-->

#### 25.5 - Présidence de l'assemblée

L’assemblée est présidée par l'un ou l'une des gérant·es tiré·e au sort. 
En cas d’absence de gérant·e, l’assemblée est présidée par un·e associé·e tiré·e au sort.

#### 25.6 - Pouvoirs

Un·e associé·e empêché·e de participer personnellement à l'_Assemblée Générale_ ne peut se faire représenter que par un·e autre associé·e.
Le nombre de procurations détenues par un·e même associé·e ne peut être supérieur à un, conformément à l’[article 14 de la loi 78][article 14-78].

[article 14-78]: https://www.legifrance.gouv.fr/loda/article_lc/LEGIARTI000029321011

<!-- Documentation
Les règles suivantes doivent être respectées :
- Si la SCOP comprend moins de 20 associé.e.s : un.e associé.e ne peut disposer que d’un pouvoir ;
- Si la SCOP comprend au moins 20 associé.e.s : un.e associé.e ne peut disposer, en plus de sa propre voix, d’un nombre de voix excédant le vingtième des associé.e.s.
Cette limitation ne s’applique pas aux pouvoirs sans désignation de mandataires visés à l’alinéa suivant.
Les pouvoirs adressés à la Société sans désignation d’un.e mandataire sont comptés comme exprimant un vote favorable à l’adoption des seules résolutions présentées ou soutenues par la gérance et défavorable à l’adoption des autres projets de résolutions.
-->

### Article 26 - Délibérations et majorité requise pour l’adoption des décisions collectives

En SARL, les décisions ordinaires ne sont pas soumises à un quorum et sont adoptées à la majorité des voix de la totalité des voix des associé·es qui peuvent être exprimées au sein de la société.

Si la première assemblée n'a pu décider dans les conditions fixées au premier alinéa, une seconde assemblée sera réunie et les décisions seront prises à la majorité des votes émis par les associé·es présent·es ou représenté·es, quel que soit le nombre de votant.

Le quorum et la majorité sont définis par les articles [L223-29][L223-29] et [L223-30][L223-30] du code du commerce.

Les abstentions, votes blancs et nuls, sont comptés comme des votes défavorables à la résolution soumise au vote. 

[L223-29]: https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000038799356
[L223-30]: https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000038799345

<!-- Documentation
Ce que prévoit la loi :

- **Décisions ordinaires** :
    - première consultation :
        - Quorum : aucune condition de quorum n’est exigée.
        - Majorité : les décisions de l’assemblée des associés doivent être prises par une majorité représentant plus de la moitié du nombre total d’associés.  
    - deuxième consultation :
Si la première assemblée n’a pu décider dans les conditions fixées au premier alinéa, une seconde assemblée sera réunie et les décisions seront prises à la majorité des présents ou représentés.  

Les décisions concernant la nomination ou la révocation du gérant sont toujours prises à la majorité absolue de l’ensemble des associés et à bulletins secrets.  

- **Décisions extraordinaires**  
    - première consultation :  
        - Quorum : les trois quarts du total des droits de vote.
        - Majorité : les trois quarts du total des droits de vote présents ou représentés.
  
    - Deuxième consultation :  
        - Quorum : la moitié du total des droits de vote.
        - Majorité : les trois quarts du total des droits de vote présents ou représentés.


Les modifications des statuts sont décidées par une majorité représentant les trois quarts du total des droits de vote présents ou représentés.
-->

<!-- Documentation
Le code du commerce déclarant que "toute clause exigeant une majorité plus élevée est réputée non écrite", on ne peux pas écrire que l’on prend des décisions à l’unanimité et espérer que ça produise un effet.

De fait, on ne peut pas augmenter la majorité.
Dans les SAS, on aurait plus de liberté de rédaction statutaire, on pourrait se permettre de mettre une gestion par consentement.
-->

## Comptes sociaux et répartition des bénéfices

### Article 27 - Dates d’ouverture et de clôture des exercices sociaux

L’exercice s’ouvre le 1<sup>er</sup> janvier et se clôture le 31 décembre.

Par exception, le premier exercice commencera à dater de l'immatriculation de la Société au Registre du Commerce et se terminera le 31 décembre 2024.

### Article 28 - Documents sociaux

Le bilan, le compte de résultat et l’annexe de la Société sont établis par la gérance et soumis à l’_Assemblée Générale Ordinaire_.

### Article 29 - Excédents nets de gestion

Les excédents nets de gestion sont décrits par l'[article 32 de la lois 1978](https://www.legifrance.gouv.fr/loda/article_lc/LEGIARTI000029321336)

L'ensemble des lois commerciales et comptables s’appliquent, en particulier les articles [L.123-12 à L.123-24](https://www.legifrance.gouv.fr/codes/section_lc/LEGITEXT000005634379/LEGISCTA000006178754/#LEGISCTA000006178754) et [R.123–172 à R.123-208](https://www.legifrance.gouv.fr/codes/section_lc/LEGITEXT000005634379/LEGISCTA000006161453/#LEGISCTA000006161453) du code de commerce.

<!-- Documentation
Le résultat est constitué par les produits de l'exercice, y compris les produits exceptionnels et sur exercices antérieurs et diminués des charges amortissements, provisions, pertes exceptionnelles, pertes sur exercices antérieurs et impôts.

Les excédents nets de gestion sont constitués par les produits nets de l'exercice, sous déduction des frais généraux et autres charges de la société, y compris tous amortissements et provisions. Ni le montant des réévaluations pratiquées sur les actifs immobilisés, ni les plus-values constatées à l'occasion de la cession de titres de participation, de la cession ou de l'apport en société de biens immobiliers, de branches d'activité ou de fonds de commerce n'entrent dans les excédents nets de gestion mentionnés au 3° de l'article 33 et ne peuvent faire l'objet d'aucune distribution aux salariés ou d'aucun versement d'intérêt aux parts. Ces plus-values sont affectées à la réserve légale et au fonds de développement.

Pour déterminer les excédents nets de gestion à partir du compte résultat, il convient :
- de déduire les reports déficitaires antérieurs ;
- de déduire les plus-values constatées à l’occasion de la cession de titres de participation, de la cession ou de l’apport en société de biens immobiliers, de branches d’activité ou de fonds de commerce, dont le montant après paiement de l'impôt est affecté à la réserve légale et au fonds de développement.
- de déduire le montant de la provision pour investissement lorsqu'elle a été constituée par dotation à poste spécial, lors de l'arrêté des comptes du sixième exercice précédent et qui est réintégrée au compte résultat à l'issue de ce délai.

En cas de réévaluation pratiquée sur les actifs immobilisés, l'écart enregistré n'entre ni dans le compte de résultat, ni dans les excédents nets de gestion
-->

<!--
La provision pour investissement définitivement libérée à l'expiration du délai visé à l'article L. 3324-10 du code du travail, ou rapportée au bénéfice imposable dans les conditions prévues au deuxième alinéa de l'article L. 442-9 du même code, est affectée à un compte de réserves exceptionnelles et n'entre pas dans les excédents nets de gestion.-->

### Article 30 - Répartition des excédents nets

La clé de répartition est variable. La décision de répartition est prise par la gérance avant la clôture de l’exercice et communiquée aux associé·es lors d’une _Assemblée Générale Ordinaire_ réunie extraordinairement ou par lettre remise en main propre contre décharge. Elle est ratifiée par l’_Assemblée Générale Ordinaire_ appelée à statuer sur les comptes de l’exercice.

La répartition est effectuée conformément à [l’article 33 de la loi 78][article 33].

La répartition de la part travail entre les bénéficiaires s’opère uniquement au prorata du temps de travail fourni.

Les bénéficiaires sont tou·tes les travailleur·euses associé·es ou non, salarié·es dans la Société et comptant à la clôture de l’exercice, soit trois mois de présence dans l’exercice, soit six mois d’ancienneté dans la Société.

[article 33]: https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000029320978

<!-- Documentation
Les règles suivantes doivent être respectées
- Réserve légale : 15% sont affectés à la réserve légale qui reçoit cette dotation jusqu’à ce qu’elle
soit égale au montant le plus élevé atteint par le capital.
- Fonds de développement : Le fonds de développement doit être doté chaque année. Une délibération en assemblée général extraordinaire pourra en déterminer la fraction d’ENG abondée pour l’exercice en cours et les suivants.
- Ristourne aux salariés : au moins égal à 25 % des ENG
-->

<!-- Documentation
Deux possibilités : 
- Une variante variable (par défaut), nécessite de s’accorder chaque fin d’année sur la clé de répartition à adopter sur le résultat : part travail (max. 84%) vs. réserve.
- Une variante avec une clé fixe qui doit être écrite dans les statuts (option non retenue ici).
Dans la grande majorité des cas, pour les SCOP en création, on laisse l’AG fixer la clé de répartition (part travail et réserve ne sont pas soumises à impôt).

Une fois que la réserve est suffisamment remplie et qu’elle permet la pérennité de l’entreprise, alors on peut la fixer à 84/16 qui permet de donner le maximum aux salarié·es et plus tellement en réserve.

La part travail est accessible aux salarié·es au-delà de 3 mois passés dans l’entreprise au cours de l’exercice social.
-->

### Article 31 - Intérêts aux parts sociales

<!-- Question
Est-ce qu'on reduit un peu ce texte pour qu'il soit plus lisible ?
-->
Il pourra être attribué un intérêt aux parts sociales. Le total des intérêts ne peut chaque année être supérieur, ni au total de la répartition aux travailleur·euses ci-dessus définie, ni au montant affecté aux réserves (réserve légale et fonds de développement).
 
Les parts sociales ouvrant droit à rémunération sont celles qui existaient au jour de la clôture de l’exercice et existent toujours à la date de l’_Assemblée Générale Ordinaire_.
 
En cas de cession, sauf stipulation contraire prévue dans l’acte de cession, c’est le détenteur des parts sociales au jour de l’_Assemblée Générale Ordinaire_ qui a droit à la rémunération.
 
Le taux d’intérêt est le même pour toutes les parts sociales.
 
Le versement des intérêts aux parts sociales a lieu, sauf affectation à la création de nouvelles parts sociales, au plus tard neuf mois après la clôture de l'exercice.

<!-- Documentation
### Article 32 - Accord de participation
#### 32.1 - Possibilité légale
S’il a été conclu un accord pour la participation des salarié.e.s aux résultats de l’entreprise :
- l’attribution aux travailleur.euse.s peut, selon les termes de cet accord, être affectée en tout ou partie à la réserve spéciale de participation des salarié.e.s ;
- les dotations faites sur les résultats d’un exercice, à la réserve légale et au fonds de développement, peuvent tenir lieu de la provision pour investissement (PPI) que la Société peut constituer à hauteur de la participation revenant aux salarié.e.s sur les résultats du même exercice.
#### 32.2 - Comptabilisation de la reserve spéciale de participation
Si la Société utilise les possibilités rappelées ci-dessus, les règles de comptabilisation suivantes s’appliqueront :
- la réserve spéciale de participation et les réserves tenant lieu de PPI ne feront pas l’objet d’une comptabilisation avant la détermination du
résultat dont elles font partie ;
- le compte de résultat devra être subdivisé de manière à faire apparaître distinctement le montant de la réserve spéciale de participation et le
montant de la réserve légale et du fonds de développement tenant lieu de PPI ;
- la réserve spéciale de participation et les réserves tenant lieu de PPI seront déduites du résultat fiscal lors de la clôture des comptes de l’exercice (tableau 2058 AN déductions diverses) ;
- la liasse fiscale comprendra les informations complémentaires définies par la lettre du Service de la Législation Fiscale à la Confédération des SCOP en date du 01.10.1987.
-->

### Article 32 - Affectation des répartitions à la création de nouvelles parts et compensation

L'assemblée des associé·es, dans les conditions prévues pour les décisions ordinaires, peut décider que la part travail et les intérêts aux parts sociales revenant aux associé·es et qui n’auront pas été affectées selon le cas, à l’exécution des engagements statutaires de souscription qui peuvent être prévus par les présents statuts, sont employées, en tout ou partie, à la création de nouvelles parts sociales.

Les droits de chaque associé·e dans l’attribution des parts sont identiques à ceux qu’il aurait eus dans la distribution des excédents nets de gestion.

### Article 33 - Impartageabilité des réserves

Quelle que soit leur origine ou leur dénomination, les réserves ne peuvent jamais être incorporées au capital et donner lieu à la création de nouvelles parts ou à l'élévation de la valeur nominale des parts, ni être utilisées pour libérer les parts souscrites, ni être distribuées, directement ou indirectement, au cours de l’existence de la Société ou à son terme, aux associés ou travailleurs de celle-ci ou à leurs héritiers et ayants droit. 

## Expiration – Dissolution – Liquidation

### Article 34 - Perte de la moitié en capital

Si du fait des pertes constatées dans les documents comptables, l’actif net devient inférieur à la moitié du capital social, la gérance doit convoquer les associé·es en assemblée qui statuera à la majorité requise pour la modification des statuts, à l’effet de décider s’il y a lieu de prononcer la dissolution de la Société ou d’en poursuivre l’activité. La résolution de l’assemblée fait l’objet d’une publicité.

### Article 35 - Expiration de la société – dissolution

À l’expiration de la société ou en cas de dissolution anticipée, l’assemblée générale règle la liquidation conformément à la loi, et nomme un ou plusieurs liquidateurs.
Après l’extinction du passif, paiement des frais de liquidation et, s’il y a lieu, des répartitions différées, les associé·es n’ont droit qu’au remboursement de la valeur nominale de leurs parts, sous déduction, le cas échéant, de la partie non libérée de celle-ci.

### Article 36 - Adhésion à la Confédération Générale des SCOP

La Société adhère à la Confédération Générale des Scop, association régie par la loi du 1<sup>er</sup> juillet 1901 dont le siège est à Paris 17<sup>e</sup>, 30, rue des Epinettes, chargée de représenter le Mouvement Coopératif et de la défense de ses intérêts, à l’Union Régionale des Scop territorialement compétente et à la Fédération professionnelle dont la Société relève.

### Article 37 - Arbitrage

Toutes les contestations qui pourraient s’élever au cours de la vie de la Société ou de sa liquidation seront soumises à la Commission d’arbitrage de la Confédération générale des Scop. Les contestations concernées sont celles pouvant s’élever :

- entre les associé·es ou ancien·nes associé·es eux-mêmes ou elles-mêmes au sujet des affaires sociales, notamment de l’application des présents statuts et tout ce qui en découle, ainsi qu’au sujet de toutes les affaires traitées entre la Société et ses associé·es ou ancien·nes associé·es ;
- entre la Société et une autre Société, soit au sujet des affaires sociales ou de toute autre affaire traitée.

La présente clause vaut compromis d’arbitrage.

Le règlement d’arbitrage est remis aux parties lors de l’ouverture de la procédure. Les sentences arbitrales sont exécutoires et susceptibles d’appel devant la Cour d’Appel de Paris.

<!-- Documentation
Indispensable pour adhérer à la CGScop : 

On doit insérer la clause d’arbitrage qui permet de soustraire tout litige aux commissions étatiques et de la soumettre à la CGScop.

En première instance, on n’irait pas au tribunal mais on irait devant la commission de la CGScop qui est compétente pour tous les litiges qui relèvent des affaires coopératives. Les juges sont souvent peu habitués, d’où l’intérêt de faire appel à la CGSCop.

Nous avons un mode de résolution des conflits qui nous est propre mais il est important de rappeler que nous avons cette opportunité.
-->

<!-- Colophon

Ces statuts ont été écrits en markdown, les commentaires sont des commentaires HTML qui nous permettent de pouvoir avoir un rendu final pour le Greffe qui soit propre et en parallèle de produire ce document commenté pour partager notre expérience/savoir dans le domaine (et ne pas oublier nous-même !).

-->

### Article 38 - Boni de liquidation

Le boni de liquidation sera attribué à la discrétion du liquidateur, à une oeuvre d'intérêt général, à la Confédération Générale des Sociétés Coopératives de Production, à une ou plusieurs coopératives de production, à une union ou fédération de coopératives de production ou collectivité territoriale.

## Personnalité morale et actes accomplis antérieurement à la consititution ou à l’immatriculation de la société 

### Article 39 - Jouissance de la personnalité morale

Conformément à la loi, la Société ne jouira de la personnalité morale qu'à dater de son immatriculation au registre du commerce et des sociétés.

La gérance de la Société est tenu, dès à présent, de remplir toutes les formalités nécessaires pour que cette immatriculation soit accomplie dans les plus courts délais.

### Article 40 - Actes accomplis pour le compte de la société en formation

Il a été accompli, dès avant ce jour, par Mme Audrey Bramy, Mme Hélène Queyssalier ou M Arthur Lenoir, pour le compte de la Société en formation les actes énoncés dans un état annexé aux présentes (Annexe I) indiquant pour chacun d'eux l'engagement qui en résultera pour la Société (Annexe I).

Les soussigné·es déclarent approuver ces engagements et la signature des statuts emportera pour la Société reprise des engagements.

Les soussigné·es conviennent que, jusqu'à ce que la Société ait acquis la jouissance de la personnalité morale, les actes et engagements entrant dans l'objet social seront accomplis ou souscrits par Mme Audrey Bramy, Mme Hélène Queyssalier ou M Arthur Lenoir appelé à exercer la gérance.

Si cette condition est remplie, elle emportera de plein droit reprise par la Société, lorsqu'elle aura été immatriculée au registre du commerce, desdits actes ou engagements qui seront réputés avoir été souscrits dès l'origine de la Société.

### Article 41 - Mandat pour les actes à accomplir pour le compte de la société en cours d’immatriculation

Dès à présent, les soussigné·es décident la réalisation immédiate, pour le compte de la Société, de différents actes et engagements

Les pouvoirs à cet effet font l’objet d’une annexe aux présentes (annexes II)

### Article 42 - Frais

Tous les frais, droits et honoraires entraînés par le présent acte et ses suites incomberont conjointement et solidairement aux soussigné·es, au prorata de leurs apports, jusqu'à ce que la société soit immatriculée au registre du commerce et des sociétés.

À compter de son immatriculation, ils seront entièrement pris en charge par la Société qui devra les amortir avant toute distribution de bénéfices, et au plus tard dans le délai de cinq ans.
